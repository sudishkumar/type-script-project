// when we define literal value then only you can put that value from declare type you can see passing literal type sudish
function combine(a, b, literal) {
    if (literal === "as-number") {
        return +a + +b;
    }
    else {
        return a.toString() + b.toString();
    }
}
console.log(combine(12, 23, "as-number"));
console.log(combine(1231, 24, "as-sudish"));
